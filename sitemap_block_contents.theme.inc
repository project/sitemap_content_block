<?php

/**
 * @file
 * Sitemap theme functions.
 */

use Drupal\Core\Template\Attribute;
use Drupal\Core\Url;
use Drupal\Core\Link;


function template_preprocess_sitemap_block_contents(array &$variables) {

  $block_load  = Drupal\block\Entity\Block::load('sitemapcontents');

  $fields_value = $block_load->get('settings')['sitemap_contents']['content_type'];

  foreach ($fields_value as $key => $value) {
    if($fields_value[$key]['value']){
      $variables['sitemapcontent'][$key]['content'] = _get_content_view($key, $key.'_sitemap');
      $variables['sitemapcontent'][$key]['title'] = $fields_value[$key]['toggle'];
    }
  }

}
