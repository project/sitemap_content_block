<?php

namespace Drupal\sitemap_block_contents\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Annotation\Translation;
use Drupal\Core\Session\AccountInterface;

use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Form\FormStateInterface;


/**
 * Provides the Sitemap in a block.
 *
 * @Block(
 *   id = "sitemap_contents",
 *   label = @Translation("Sitemap Contents"),
 *   admin_label = @Translation("Sitemap Contents")
 * )
 */
class SitemapContentsBlock extends BlockBase implements BlockPluginInterface {


  /**
   * {@inheritdoc}
   */
  public function build() {
    return array(
      '#theme' => 'sitemap_block_contents'
    );
  }


  public function blockForm($form, FormStateInterface $form_state)
  {
    $form = parent::blockForm($form, $form_state);

    $config = $this->getConfiguration();

    $form['sitemap_contents'] = array(
      '#type' => 'details',
      '#title' => $this->t('Contents Type'),
      '#open' => TRUE,
    );

    $types = \Drupal::entityTypeManager()
      ->getStorage('node_type')
      ->loadMultiple();

      foreach ($types as $key => $value) {

        $form['sitemap_contents']['content_type'][$key] = array(
          '#type' => 'details',
          '#title' => $this->t($key),
          '#open' => FALSE,
        );

          $form['sitemap_contents']['content_type'][$key]['value'] = [
            '#type' => 'checkbox',
            '#title' => $this->t($key),
            '#default_value' => isset($config['sitemap_contents']['content_type'][$key]['value']) ? $config['sitemap_contents']['content_type'][$key]['value'] : '',
          ];

        $form['sitemap_contents']['content_type'][$key]['toggle']= [
          '#type' => 'textfield',
          '#title' => $this->t('Title'),
          '#default_value' => isset($config['sitemap_contents']['content_type'][$key]['toggle']) ? $config['sitemap_contents']['content_type'][$key]['toggle'] : '',
        ];
      }

    return $form;
  }




  public function blockSubmit($form, FormStateInterface $form_state)
  {
    parent::blockSubmit($form, $form_state);
    $values = $form_state->getValues();
    $this->configuration['sitemap_contents']['content_type'] = $values['sitemap_contents']['content_type'];
  }

}
